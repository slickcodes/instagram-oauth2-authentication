<?php
	// add required classes 
	require_once('config.php');
	require_once('class.instagram.php');
	
	// initialize config
	$config = new ConfigClass();

	// initialize instagram
	$api = $config->instagram();
	$insta = new InstagramClass($api['id'], $api['secret']); 

?>