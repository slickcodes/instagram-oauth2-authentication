<?php
	require_once('autoload.php');
?>


<!doctype html>
	<head>
		<style>
			/* elements */
			html,body{
				background-color: #fafafa;
				font-family: arial;
			}

			*{
				box-sizing: border-box;
			}

			

			/* classes */
			.content{
				max-width: 350px;
				
			}
			
			.block{

			}

			.center{
				margin:0 auto;
			}

			.button{
				background: #3897f0;
			    border-color: #3897f0;
			    color: #fff;
			    border-style: solid;
			    border-radius: 3px;
			    width:100%;
			    padding:5px;
			    font-size: 14px;
			    cursor: pointer;
			    text-decoration: none;
			    display: inline-block;
			    text-align: center;
			}

			.instagram-text{
				background-image: url(//instagramstatic-a.akamaihd.net/h1/sprites/core/3a91ff.png);
			    background-repeat: no-repeat;
			    background-position: -258px -268px;
			    height: 51px;
			    width: 175px;
			}
			

			.login-container{
				background-color: #ffffff;
				border: 1px solid #efefef;
				padding:20px;
			}

			.login-button{
				margin-top:20px;
			}

			.disclaimer-container{
				color:#999;
				font-size: 12px;
				text-align: center;
				margin-top:20px;
			}

		</style>
	</head>
	<body>
		<header></header>
		<main>
			<div class='center content'>
				<div class='block'>
					<div class='login-container'>					
						<h1 class='instagram-text center'></h1>						
						<a class='button login-button' href='<?php echo $insta->authenticate(); ?>'>Log in</a>
					</div><!-- end of login-container -->
					<div class='disclaimer-container'>
						This is an instagram api
						<?php echo $_POST['code']; ?>
					</div><!-- end of disclaimer-container -->
				</div>
			</div>
		</main>
		<footer></footer>
	</body>
</html>