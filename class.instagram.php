<?php
	class InstagramClass
	{
		private $id = '';
		private $secret = '';
		private $token = '';
		private $host = 'https://api.instagram.com';
		//private $redirect = 'http://localhost/instagram/authorize.php';
		private $redirect = 'http://instagram.slickcodes.com/authorize.php';
		private $code = '';
		
		// this constructor sets the instagram's client credentials
		public function __construct($insta_id, $insta_secret)
		{
			$this->id = $insta_id;
			$this->secret = $insta_secret;
		}
		
		// this function will authorize user and will return authorization code
		public function authenticate()
		{
			$url  = $this->host.'/oauth/authorize/?';
			$url .= 'client_id='.$this->id;
			$url .= '&redirect_uri='.$this->redirect.'&response_type=code';
			
			return $url;
		}

		// this function stores the authentication code
		public function set_authentication_code($code){
			$this->code = $code;
		}

		// this function retrieve and returns the authentication code
		public function get_authentication_code(){
			return $this->code;
		}

		// this function will obtain an access token
		public function authorize($api){
			// url to make the call
			$url = $this->host.'/oauth/access_token';

			$data = array(
				'client_id' => $api['id'],
				'client_secret' => $api['secret'],
				'grant_type' => 'authorization_code',
				'code' => $api['code'],
				'redirect_uri' => 'http://instagram.slickcodes.com/authorize.php'
			);

			$response = $this->curl($url, $data);

			return json_decode($response);
		}

		// this function will store user's access token
		public function set_access_token($token){
			$this->token = $token;
		}


		// this function will return user's recent photos
		public function get_recent_photos(){
			$url = $this->host.'/v1/users/self/media/recent/?access_token='.$this->token;
			
			$response = $this->curl($url);
			
			return $this->success($response);
		}
		
		// this function will make a curl call and return the response as json object
		private function success($data){
			
			// decode to json
			return json_decode($data, true);
		}

		// this function is a common curl call
		private function curl($url, $data = NULL){
			$request = curl_init($url); // initiate curl object
			
			curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
			curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
			
			// check if the paramater is null
			if( !is_null($data) ){
				curl_setopt($request, CURLOPT_POSTFIELDS, $data); // use HTTP POST to send form data
			}

			$response = curl_exec($request); // execute curl post and store results in $response
			
			curl_close($request); // close curl object

			return $response;
		}

	}
?>