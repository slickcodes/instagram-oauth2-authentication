<?php
	// display authentication code
	if(empty($_GET['code']) ){
		header('Location: http://instagram.slickcodes.com');
		//header('Location: http://localhost/instagram');
		exit();
	}

	require_once('autoload.php');
	
	// get code
	$api['code'] = $_GET['code'];

	// save authentication code
	$insta->set_authentication_code($code);

	// make curl call to get access token
	$response = $insta->authorize($api);

	// save OAuth Token.
	$insta->set_access_token($response->access_token);
	
	// make api call
	$recent =  $insta->get_recent_photos();

	// populate and style the objects
	if($recent['meta']['code'] === 200){

		echo '<h1>Most recent photos</h1>';
		foreach($recent['data'] as $data){
			echo '<img src="'.$data['images']['low_resolution']['url'].'" />';
		}
	}

?>

<style>
	img{
		float:left;
		margin:1px;
	}
</style>